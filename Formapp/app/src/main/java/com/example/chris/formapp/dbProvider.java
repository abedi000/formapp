package com.example.chris.formapp;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.util.Log;

public class dbProvider extends ContentProvider {
    public final static String DBNAME = "NameDatabase";
    public final static String TABLE_NAMESTABLE = "Employees";
    public final static String COLUMN_EMPLID = "emplid";

    private static final String SQL_CREATE_MAIN =
            "CREATE TABLE "+ TABLE_NAMESTABLE + "(" +
                    " _ID INTEGER PRIMARY KEY, " +
                    COLUMN_EMPLID + " TEXT, " +
                    "Name TEXT, " +
                    "Gender TEXT, " +
                    "Email TEXT," +
                    "Access TEXT, "+
                    "Department TEXT, " +
                    "Ad TEXT )";

    public static final Uri AUTHORITY =
            Uri.parse("content://com.example.chris.formapp.provider");
    public static final Uri CONTENT_URI = Uri.parse(
            "content://com.example.chris.formapp.provider/" + TABLE_NAMESTABLE);

    MainDatabaseHelper  mOpenHelper;
    private static UriMatcher sUriMatcher;
    protected static final class MainDatabaseHelper extends SQLiteOpenHelper {
        MainDatabaseHelper(Context context) {
            super(context, DBNAME, null, 1);
        }
        @Override
        public void onCreate(SQLiteDatabase db) {db.execSQL(SQL_CREATE_MAIN);}

        //public void delete(SQLiteDatabase db){db.execSQL("delete from "+ TABLE_NAMESTABLE);}
        @Override
        public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
        }
    }




    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        // Implement this to handle requests to delete one or more rows.
        return mOpenHelper.getWritableDatabase().delete(TABLE_NAMESTABLE, selection, selectionArgs);
    }

    @Override
    public String getType(Uri uri) {
        // TODO: Implement this to handle requests for the MIME type of the data
        // at the given URI.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        String emplid = values.getAsString(COLUMN_EMPLID).trim();
      //  String lname = values.getAsString(COLUMN_LASTNAME).trim();

        if (emplid.equals(""))
            return null;

        long id = mOpenHelper.getWritableDatabase().insert(TABLE_NAMESTABLE, null, values);

      //  Log.i("hrre","In Insert");
        return Uri.withAppendedPath(CONTENT_URI, "" + id);
    }

    @Override
    public boolean onCreate() {
        // TODO: Implement this to initialize your content provider on startup.
        mOpenHelper = new MainDatabaseHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        return mOpenHelper.getReadableDatabase().query(TABLE_NAMESTABLE, projection, selection, selectionArgs,
                null, null, sortOrder);
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        // TODO: Implement this to handle requests to update one or more rows.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
