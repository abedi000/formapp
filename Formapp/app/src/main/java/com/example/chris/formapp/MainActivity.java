package com.example.chris.formapp;

import android.content.ContentValues;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    Button resetButton;
    Button submitButton;
    Button addButton;


    EditText emplid;
    EditText editText2;
    EditText editText3;
    EditText editText4;
    EditText editText5;
    RadioGroup rg;
    Spinner spinner;
    Cursor mCursor;
    CheckBox checkbox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.linear);
        resetButton = (Button)
                findViewById(R.id.resetButton);
        submitButton = (Button)
                findViewById(R.id.submitButton);
        addButton = (Button)
                findViewById(R.id.addButton);

        editText2 = (EditText) findViewById(R.id.editText2);
        editText3 = (EditText) findViewById(R.id.editText3);
        editText4 = (EditText) findViewById(R.id.editText4);
        editText5 = (EditText) findViewById(R.id.editText5);
        emplid = (EditText) findViewById(R.id.emplid);
        rg = (RadioGroup) findViewById(R.id.rg);
        spinner = (Spinner) findViewById(R.id.spinner);
        checkbox = (CheckBox) findViewById(R.id.checkBox);


    }


    public void myEventHandler(View v) {


        if (v == addButton) {

            String check = "no";
            String gender = "M";
            if (rg.getCheckedRadioButtonId() == R.id.radioButton2)
                gender = "F";

            if (checkbox.isChecked())
                check = "yes";

            Uri mNewUri;
            ContentValues mNewValues = new ContentValues();
            mNewValues.put(dbProvider.COLUMN_EMPLID, emplid.getText().toString().trim());
            mNewValues.put("Name", editText2.getText().toString().trim());
            mNewValues.put("Gender", gender);
            mNewValues.put("Email", editText3.getText().toString().trim());
            mNewValues.put("Access", editText4.getText().toString().trim());
            mNewValues.put("Department", spinner.getSelectedItem().toString());
            mNewValues.put("Ad", check);

            mCursor = getContentResolver().query(dbProvider.CONTENT_URI, null, null, null, null);

            if (mCursor != null) {
                Boolean canadd = true;
                while (mCursor.moveToNext()) {
                    if (emplid.getText().toString().trim().equals(mCursor.getString(1)))
                        canadd = false;
                 //   Log.i("id", mCursor.getString(0));
                 //   Log.i("Emplid", mCursor.getString(1));
                 //   Log.i("Name", mCursor.getString(2));
                  //  Log.i("gender", mCursor.getString(3));
                  //  Log.i("email", mCursor.getString(4));
                   // Log.i("access", mCursor.getString(5));
                   // Log.i("dept", mCursor.getString(6));
                   // Log.i("ad", mCursor.getString(7));

                }
                if (!canadd) {
                    Toast.makeText(
                            this, "Employee Id is a duplicate",
                            Toast.LENGTH_LONG).show();
                }
                if (canadd)
                    mNewUri = getContentResolver().insert(
                            dbProvider.CONTENT_URI, mNewValues);

            }


            Log.i(emplid.getText().toString(), "here in ADD");


        }
        if (v == resetButton) {
            clear();
        }

        if (v == submitButton) {
            Uri mNewUri;

            String pass = editText4.getText().toString();
            String confirm = editText5.getText().toString();

            if (pass.equals(confirm)) {

                mCursor = getContentResolver().query(dbProvider.CONTENT_URI, null, null, null, null);

                if (mCursor != null) {
                    Boolean canadd = true;
                    while (mCursor.moveToNext()) {
                       // Log.i("id", mCursor.getString(0));
                      //  Log.i("Emplid", mCursor.getString(1));
                      //  Log.i("Name", mCursor.getString(2));
                      //  Log.i("gender", mCursor.getString(3));
                      //  Log.i("email", mCursor.getString(4));
                      //  Log.i("access", mCursor.getString(5));
                      //  Log.i("dept", mCursor.getString(6));
                      //  Log.i("ad", mCursor.getString(7));
                        if (emplid.getText().toString().trim().equals(mCursor.getString(1)))
                            canadd = false;
                    }

                    if (canadd) {
                        Toast.makeText(
                                this, "This Employee is not in the Database",
                                Toast.LENGTH_LONG).show();

                    }
                    if (!canadd) {
                        Toast.makeText(
                                this, "The Form was Successfully Submitted",
                                Toast.LENGTH_LONG).show();

                    }
                }

            }


        }
    }

    private void clear() {
        rg.clearCheck();
        editText2.setText("");
        editText3.setText("");
        editText4.setText("");
        editText5.setText("");
        emplid.setText("");
        checkbox.setChecked(false);
        mCursor = null;
    }


    private void deleteall()
    {
        getContentResolver().delete(dbProvider.CONTENT_URI,null,null);

    }
}

